<?php

class Bootstrap
{
    private static $_run_once = false;

    public static function buildPath () {
        $fullPath = func_get_arg(0);

        for($i=1;$i<count(func_get_args());$i++) {
            $fullPath .= DIRECTORY_SEPARATOR . func_get_args()[$i];
        }
        return $fullPath;
    }

    private static function autoloader( $class ) {
        $file = self::buildPath(constant("PROJECTROOT"), str_replace('\\', DIRECTORY_SEPARATOR , $class) . '.php');
        if (file_exists($file)) {
            require $file;
        }
    }

    public static function exceptionHandler($exception ) {
        // Later this will be redirected to a layout that will interact with the error directly to control this better
        print get_class($exception) . " '{$exception->getMessage()}' in {$exception->getFile()}({$exception->getLine()})\n"
            . "{$exception->getTraceAsString()}";
        die();
    }

    private static function iniToConstants() {
        $configPath = self::buildPath(constant("PROJECTROOT"), 'settings', 'config.ini');
        $ini_vars = parse_ini_file($configPath);
        foreach ($ini_vars as $const => $val) {
            define($const,$val);
        }
        define('publicBaseURL', (isset($_SERVER['HTTPS']) ? "https" : "http") . "://$_SERVER[HTTP_HOST]");
    }

    public static function init () {
        if (Bootstrap::$_run_once === false) {
            Bootstrap::$_run_once = true;
        } else {
            throw new Exception("Bootstrap should only be run once");
        }

        define("PROJECTROOT",__DIR__);

        spl_autoload_register('Bootstrap::autoloader');

        self::iniToConstants();

        $frontController = \lib\Controller\FrontController::getFrontController();
        $frontController->handleRequest();
    }

}

?>