# Last FM Client
## Written by Kieran Wilson
This project was sent to me as a challenge. It was to write a simple integration with LastFM using entirely your own code.
I received the project on a Friday morning with the goal to return it Monday morning. I went a bit overkill on this project
in terms of the needless complexity but it does showcase how I like to structure my code and within the given time I am proud
of this project.

### Requirements to run
* Web root should be set to the public_html folder
* rename the file settings/settings.ini.default to config.ini and put your last.fm api key into it

### To-Do
* Write proper 404 handler + set header for when they occur
* Proper documentation
* Unit Testing