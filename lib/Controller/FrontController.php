<?php

namespace lib\Controller;
use \lib\Path\PathHelper;
use \lib\Router\Router;

class FrontController {

    private static  $instance;
    public          $_applicationConfig;
    public          $_pathHelper;
    private         $_controller;
    private         $_action;
    public          $modules;

    public static function getFrontController() {
        if (!isset(self::$instance)) {
            $class = __CLASS__;
            self::$instance = new $class();
        }
        return self::$instance;
    }

    public function getPathHelper() {
        return $this->_pathHelper;
    }

    public function exceptionHandler($exception ) {
        // Later this will be redirected to a layout that will interact with the error directly to control this better
        print get_class($exception) . " '{$exception->getMessage()}' in {$exception->getFile()}({$exception->getLine()})\n"
            . "{$exception->getTraceAsString()}";
        die();
    }

    private function __construct() {
        set_exception_handler(array($this, 'exceptionHandler'));
        $class = __CLASS__;
        $this->_pathHelper = new PathHelper();

        $applicationConfig = (include $this->_pathHelper->BuildPath(constant("PROJECTROOT"),
            'application', 'application.config.php'));
        $this->setApplicationConfig($applicationConfig);
        $this->modules = $this->getApplicationConfig('modules');
    }

    function usortCmp($a, $b)
    {
        if ($a['priority'] == $b['priority']) {
            return 0;
        }
        return ($a['priority'] < $b['priority']) ? -1 : 1;
    }

    public function setApplicationConfig($applicationConfig) {
        $this->_applicationConfig = $applicationConfig;
    }

    public function getApplicationConfig($key) {
        return $this->_applicationConfig[$key];
    }

    public function getModuleDirectory($module) {
        return $this->_pathHelper->BuildPath(constant("PROJECTROOT"), 'application', $module);
    }

    public function handleRequest() {
        $router = Router::getRouter();
        $route = $router->routeToController();

        if ($route === false) {
            Throw New \Exception('404');
        }

        $this->loadController($route);
    }

    public function loadController($route) {
        $this->_controller = $route['controller'];
        $this->_action = $route['action'];
        $classPath = "application\\{$this->_controller}";
        $controllerInstance = new $classPath();
        $controllerInstance->route($this->_action);
    }
}

?>