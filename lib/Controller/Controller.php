<?php

namespace lib\Controller;

use \lib\Controller\Helper\ViewHelper;

class Controller {
    protected $_viewHelper;
    protected $_action;
    protected $_module;

    public function __construct() {
        $classParts = explode('\\',get_class($this));
        $module = $classParts[count($classParts)-3];
        $this->_module = $module;
        $this->_viewHelper = new ViewHelper(get_class($this), $module);
    }

    public function route($action) {
        $this->_viewHelper->setView($action);
        $this->_action = $action;
        $viewOptions = $this->$action();
        $this->_viewHelper->display($viewOptions);
    }

}