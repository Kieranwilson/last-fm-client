<?php

namespace lib\Controller\Helper;

use lib\Controller\FrontController;
use lib\Path\PathHelper;
use lib\View\View;

class ViewHelper {
    protected $_frontController;
    protected $_view;
    protected $_layout;
    protected $_controller;
    protected $_pageTitle = NULL;
    protected $_applicationTitle;
    protected $_metaDesc;
    protected $_module;

    protected $_stylesheets = array();
    protected $_javascriptHead = array();
    protected $_javascriptFoot = array();

    const TITLESEPARATOR = '|';

    public function addJavascript($url, $priority = 10, $version = '1.0', $footer = false) {
        if ($footer === false)
            $this->_javascriptHead[] = array('url' => $url, 'version' => $version, 'priority' => $priority);
        else
            $this->_javascriptFoot[] = array('url' => $url, 'version' => $version, 'priority' => $priority);
        return $this; // allow chaining of script files
    }

    public function getHeadScripts() {
        usort($this->_javascriptHead, array($this->_frontController, 'usortCmp'));
        return $this->_javascriptHead;
    }

    public function getFootScripts() {
        usort($this->_javascriptFoot, array($this->_frontController, 'usortCmp'));
        return $this->_javascriptFoot;
    }

    public function addStylesheet($url, $priority = 10, $version = '') {
        $this->_stylesheets[] = array('url' => $url, 'version' => $version, 'priority' => $priority);
        return $this; // allow chaining of style sheets
    }

    public function getStylesheets() {
        usort($this->_stylesheets, array($this->_frontController, 'usortCmp'));
        return $this->_stylesheets;
    }

    private function orderJavascript() {
        usort($this->_javascript, array($this->_frontController, 'usortCmp'));
    }

    public function getJavascript() {
        $this->orderJavascript();
        return $this->_javascript;
    }

    public function setTopNav($topnav) {
        $this->_topnav = $topnav;
        return $this;
    }

    public function getTopNav() {
        return $this->_topnav;
    }

    public function __construct($controller, $module)
    {
        $this->setController($controller);
        $this->_frontController = FrontController::getFrontController();
        $this->_module = $module;

        $this->setApplicationTitle($this->_frontController->getApplicationConfig('appTitle'))
            ->setMetaDesc($this->_frontController->getApplicationConfig('defaultMeta'))
            ->setLayout($this->_frontController->getApplicationConfig('defaultLayout'))
            ->setTopNav($this->_frontController->getApplicationConfig('topnav'));
    }

    public function setApplicationTitle($title) {
        $this->_applicationTitle = $title;
        return $this;
    }

    public function getApplicationTitle() {
        return $this->_applicationTitle;
    }

    public function setMetaDesc($metaDesc) {
        $this->_metaDesc = $metaDesc;
        return $this;
    }

    public function getMetaDesc() {
        return $this->_metaDesc;
    }

    public function setPageTitle($pageTitle) {
        $this->_pageTitle = $pageTitle;
    }

    public function buildTitle() {
        if ($this->_pageTitle === NULL) {
            $title = $this->_applicationTitle;
        } else {
            $title = "{$this->_pageTitle} " . self::TITLESEPARATOR . " {$this->_applicationTitle}";
        }
        return $title;
    }

    public function setController($controller)
    {
        $this->_controller = $controller;
    }

    public function setView($view)
    {
        $this->_view = $view;
    }

    public function setLayout($layout)
    {
        $this->_layout = $layout;
        return $this;
    }

    public function getViewFile() {
        $path = PathHelper::BuildPath(constant("PROJECTROOT"), 'application', $this->_module, 'View', "{$this->_view}.phtml");
        return $path;
    }

    public function getLayoutFile() {
        $path = PathHelper::BuildPath(constant("PROJECTROOT"), 'application', $this->_layout, 'Layout', 'layout.phtml');
        return $path;
    }

    public function display($fields) {
        $view = new View($this, $this->getLayoutFile(), $this->getViewFile(), $fields);
        $view->render();
    }
}
