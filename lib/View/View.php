<?php

namespace lib\View;


class View
{
    protected $_viewHelper;
    protected $_layoutFile;
    protected $_viewFile;
    protected $_fields;
    protected $_content;
    protected $_docType = '<!DOCTYPE html>';

    public function outDocType() {
        return $this->_docType;
    }

    public function pageHead() {
        return $this->_viewHelper->getApplicationTitle();
    }

    public function outCSS() {
        $sheets = $this->_viewHelper->getStylesheets();
        $sheetHtml = '';
        foreach ($sheets as $sheet) {
            if ($sheet['version'] !== '')
                $sheetHtml .= "<link rel=\"stylesheet\" type=\"text/css\" href=\"{$sheet['url']}?v={$sheet['version']}\">";
            else
                $sheetHtml .= "<link rel=\"stylesheet\" type=\"text/css\" href=\"{$sheet['url']}\">";
        }
        return $sheetHtml;
    }

    public function topNav() {
        $topNav = $this->_viewHelper->getTopNav();
        $topNavHtml = '<ul class="top-nav">';
        foreach ($topNav as $navItem) {
            $content = $navItem['content'];
            unset($navItem['content']);
            $topNavHtml .= "<li><a";
            foreach ($navItem as $property => $value) {
                $topNavHtml .= " {$property}=\"{$value}\"";
            }
            $topNavHtml .= ">{$content}</a></li>";
        }
        $topNavHtml .= '</ul>';
        return $topNavHtml;
    }

    public function outJSHead() {
        $scripts = $this->_viewHelper->getHeadScripts();
        $scriptHtml = '';
        foreach ($scripts as $script) {
            if ($script['version'] !== '')
                $scriptHtml .= "<script src=\"{$script['url']}?v={$script['version']}\"></script>";
            else
                $scriptHtml .= "<script src=\"{$script['url']}\"></script>";
        }
        return $scriptHtml;
    }

    public function outJSFoot() {
        $scripts = $this->_viewHelper->getFootScripts();
        $scriptHtml = '';
        foreach ($scripts as $script) {
            if ($script['version'] !== '')
                $scriptHtml .= "<script src=\"{$script['url']}?v={$script['version']}\"></script>";
            else
                $scriptHtml .= "<script src=\"{$script['url']}\"></script>";
        }
        return $scriptHtml;
    }

    public function headTitle() {
        return "<title>{$this->_viewHelper->buildTitle()}</title>";
    }

    public function headMeta() {
        return "<meta name=\"Description\" content=\"{$this->_viewHelper->getMetaDesc()}\" />";
    }

    public function __construct($viewHelper, $layoutFile, $viewFile, $fields) {
        $this->_viewHelper = $viewHelper;
        $this->_layoutFile = $layoutFile;
        $this->_viewFile = $viewFile;
        $this->_fields = $fields;
    }

    public function render() {
        ob_start();
        include $this->_viewFile;
        $this->_content = ob_get_contents();
        ob_end_clean();
        ob_start();
        include $this->_layoutFile;
        $fullHTML = ob_get_contents();
        ob_end_clean();
        echo $fullHTML;
    }
}