<?php

namespace lib\Router;
use lib\Controller\FrontController;

class Router {
    private static $instance;
    private $_routes;
    public $frontController;

    public static function getRouter() {
        if (!isset(self::$instance)) {
            $class = __CLASS__;
            self::$instance = new $class();
        }
        return self::$instance;
    }

    private function __construct()
    {
        $this->frontController = FrontController::getFrontController();

        foreach ($this->frontController->modules as $module) {
            $modConf = $this->frontController->_pathHelper->BuildPath($this->frontController->getModuleDirectory($module), 'module.config.php');
            if (file_exists( $modConf) === true) {
                $conf = (include $modConf);
                $routes = $conf['routes'];
                if (is_array($routes) && !empty($routes))
                    $this->addRoutes($routes);
            } else {
                throw new \Exception("Module '{$module}' is missing module.config.php");
            }
        }
    }

    public function addRoute($route) {
        if (!isset($route['priority']))
            $route['priority'] = 10;
        $this->_routes[] = $route;
    }

    public function addRoutes($routes) {
        foreach ($routes as $route) {
            $this->addRoute($route);
        }
    }

    public function routeMatch($url, $route) {
        if (substr($route, -2,1) === '?')
            return true;
        else if (strtolower($url) === strtolower($route)) // easiest case insensitivity
            return true;
        return false;
    }

    private function prepareRouteRegex($route) {
        $route = trim($route, '/');
        $route = str_replace('/', "\/", $route);
        $route = "/^\/{$route}\/?$/i";
        return $route;
    }

    private function orderRoutes() {
        usort($this->_routes, array($this->frontController, 'usortCmp'));
    }

    public function routeToController() {
        $controller = false;
        $action = false;
        $this->orderRoutes();

        foreach ($this->_routes as $curRoute) {
            $route = $this->prepareRouteRegex($curRoute['route']);

            if (preg_match($route, $_SERVER['REQUEST_URI'])) {
                $controller = $curRoute['controller'];
                $action = $curRoute['action'];
                break;
            }
        }
        if ($controller === false || $action === false)
            return false;

        return array(
            'controller'    => $controller,
            'action'        => $action
        );
    }

}