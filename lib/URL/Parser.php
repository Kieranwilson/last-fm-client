<?php

namespace lib\URL;

class Parser {

    static public function returnAllParts() {
        if (isset($_SERVER['REQUEST_URI']) && $_SERVER['REQUEST_URI'] !== '/' && $_SERVER['REQUEST_URI'] !== '') {
            $url = trim($_SERVER['REQUEST_URI'],'/');

            $parts = explode('/', $url);
        } else {
            $parts = false;
        }

        return $parts;
    }

    static public function returnPart($index) {
        $part = false;
        $parts = self::returnAllParts();
        if ($parts !== false && isset($parts[$index])) {
            $part = $parts[$index];
        }
        return $part;
    }

}