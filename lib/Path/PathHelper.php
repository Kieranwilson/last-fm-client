<?php

namespace lib\Path;

class PathHelper {

    static function BuildPath() {
        $fullPath = func_get_arg(0);

        for($i=1;$i<count(func_get_args());$i++) {
            $fullPath .= DIRECTORY_SEPARATOR . func_get_args()[$i];
        }
        return $fullPath;
    }

}