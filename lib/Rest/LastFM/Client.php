<?php

namespace lib\Rest\LastFM;
use \lib\Rest\Client as RestClient;

class Client extends RestClient {
	private $_api_key;
	protected $_curlOpts = array(
        CURLOPT_RETURNTRANSFER => true
    );
	
	static public function connectAPI($api_key, $url)
    {
        $instance = new self();
        $instance->_api_key = $api_key;
        $instance->_url = $url;
        return $instance;
    }

    public function geoTopArists($country, $page = 1, $limit = 5, $json = true)
    {
        $this->_arguments['page'] = $page;
        $this->_arguments['limit'] = $limit;
        $this->_arguments['method'] = "geo.gettopartists";
        $this->_arguments['country'] = $country;
        $this->_arguments['api_key'] = $this->_api_key;
        if ($json === true) {
            $this->_arguments['format'] = 'json';
        }

        return $this->runQuery();
    }

    public function getTopTracks($artist, $page = 1, $limit = 10, $autoCorrect = 1, $json = true) {
        $this->_arguments['page'] = $page;
        $this->_arguments['limit'] = $limit;
        $this->_arguments['autocorrect'] = $autoCorrect;
        $this->_arguments['method'] = "artist.getTopTracks";
        $this->_arguments['api_key'] = $this->_api_key;
        $this->_arguments['artist'] = $artist;

        if ($json === true) {
            $this->_arguments['format'] = 'json';
        }

        return $this->runQuery();
    }
}