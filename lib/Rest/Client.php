<?php

namespace lib\Rest;

class Client {
	protected $_url;
    protected $_curlOpts = array();
    protected $_arguments = array();
	
	public function __construct() {

	}

	public function setUrl($url) {
	    $this->_url = $url;
	    return $this;
    }

    public function addCurlOpts($optArray) {
        $this->_curlOpts = array_merge($this->_curlOpts, $optArray);
    }

    public function addCurlOpt($optKey, $optVal) {
        $this->_curlOpts[$optKey] = $optVal;
    }

    public function buildQueryString() {
	    $queryString = '?';
	    foreach ($this->_arguments as $key => $value) {
            $queryString .= "{$key}={$value}&";
        }
        return substr($queryString, 0, -1);
    }

    public function runQuery() {
	    if (!isset($this->_url)) {
            throw new \Exception("Rest Client Error, URL was not set first");
        }

        $argumentString = $this->buildQueryString();
	    $this->addCurlOpt(CURLOPT_URL, $this->_url . $argumentString);

        $curl = curl_init();
	    foreach ($this->_curlOpts as $key => $value) {
            curl_setopt($curl, $key, $value);
        }

        return curl_exec($curl);
    }
	
}