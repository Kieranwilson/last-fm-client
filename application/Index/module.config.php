<?php

return array(
    // Array of routes for this module
    'routes' => array(
        array(
            'route'     => '',
            'controller'=> 'Index\Controller\Index',
            'action'    => 'index'
        ),
        array(
            'route'     => '/[^/]+',
            'controller'=> 'Index\Controller\Index',
            'action'    => 'page',
            'priority'  => 20,
        )
    )
);
