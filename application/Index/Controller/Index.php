<?php

namespace application\Index\Controller;

use \lib\Controller\Controller;
use \lib\URL\Parser as URLParser;

class Index extends Controller {

    public function index() {
        $this->_viewHelper->setPageTitle('Home');
    }

    public function page() {
        // handles top level pages
        $pageTitle = URLParser::returnPart(0);
        switch ($pageTitle) {
            case 'about':
                return $this->aboutPage();
                break;
            default:
                Throw New \Exception('404');
                break;
        }

    }

    public function aboutPage() {
        $this->_viewHelper->setView('about');
        $this->_viewHelper->setPageTitle('About this project');
    }

}

