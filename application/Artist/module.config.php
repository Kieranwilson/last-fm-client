<?php

return array(
    // Array of routes for this module
    'routes' => array(
        array(
            'route'    => '/Artist(/[^/]+)(/[^/]+)?',
            'controller' => 'Artist\Controller\Artist',
            'action'     => 'artist'
        )
    )
);
