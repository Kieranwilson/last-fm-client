<?php

namespace application\Artist\Controller;

use \lib\Controller\Controller;
use \lib\Rest\LastFM\Client as LastFMClient;
use \lib\URL\Parser as URLParser;

class Artist extends Controller {

    function handleRestError($response) {
        $this->_viewHelper->setPageTitle('Error');
        $this->_viewHelper->setView('error');

        return array(
            'response' => $response
        );
    }

    private function fixTooManyEntries($request, $limit = 10) {
        while (count($request['toptracks']['track']) > $limit) {
            array_shift($request['toptracks']['track']);
        }

        return $request;
    }

    public function artist() {
        $lastClient = LastFMClient::connectAPI(constant('api_key'), constant('lastfm_api_url'));
        $page = (URLParser::returnPart(2) !== false) ? intval(URLParser::returnPart(2)) : 1;
        $artist = rawurldecode(URLParser::returnPart(1));
        $response = json_decode($lastClient->getTopTracks(rawurlencode($artist), $page), true);

        $response = $this->fixTooManyEntries($response);

        if (isset($response['error'])) {
            return $this->handleRestError($response);
        }

        $this->_viewHelper->setPageTitle('Top Tracks for ' . $artist);

        $url = constant('publicBaseURL') . "/Artist/" . rawurlencode($artist);
        if ($page > 1)
            $prev = $url . '/' . ($page - 1);
        else
            $prev = false;

        if ($page !== $response['toptracks']["@attr"]['totalPages'])
            $next = $url . '/' . ($page + 1);
        else
            $next = false;

        return array(
            'request' => $response,
            'prev' => $prev,
            'next' => $next,
            'url' => $url,
            'artist' => $artist
        );
    }

}

