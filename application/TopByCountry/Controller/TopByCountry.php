<?php

namespace application\TopByCountry\Controller;

use \lib\Controller\Controller;
use \lib\Rest\LastFM\Client as LastFMClient;
use \lib\URL\Parser as URLParser;

class TopByCountry extends Controller {

    function handleRestError($response) {
        $this->_viewHelper->setPageTitle('Error');
        $this->_viewHelper->setView('error');

        return array(
            'response' => $response
        );
    }

    private function fixTooManyEntries($request, $limit = 5) {
        while (count($request['topartists']['artist']) > $limit) {
            array_shift($request['topartists']['artist']);
        }

        return $request;
    }

    function topByCountry() {
        $lastClient = LastFMClient::connectAPI(constant('api_key'), constant('lastfm_api_url'));
        $page = (URLParser::returnPart(2) !== false) ? intval(URLParser::returnPart(2)) : 1;
        $country = rawurldecode(URLParser::returnPart(1));
        $response = json_decode($lastClient->geoTopArists(rawurlencode($country), $page), true);

        if (isset($response['error'])) {
            return $this->handleRestError($response);
        }

        $response = $this->fixTooManyEntries($response);

        $this->_viewHelper->setPageTitle('Top Artists in ' . $country);

        $url = constant('publicBaseURL') . "/TopByCountry/" . rawurlencode($country);
        if ($page > 1)
            $prev = $url . '/' . ($page - 1);
        else
            $prev = false;

        if ($page !== $response['topartists']["@attr"]['totalPages'])
            $next = $url . '/' . ($page + 1);
        else
            $next = false;

        return array(
            'request' => $response,
            'prev' => $prev,
            'next' => $next,
            'url' => $url,
            'country' => $country
        );
    }

}

