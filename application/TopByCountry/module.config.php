<?php

return array(
    // Array of routes for this module
    'routes' => array(
        array(
            'route' => '/TopByCountry(/[^/]+)(/[^/]+)?',
            'controller' => 'TopByCountry\Controller\TopByCountry',
            'action' => 'topByCountry'
        )
    )
);