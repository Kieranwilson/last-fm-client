<?php

return array(
    // This should be an array of module for the application
    'modules' => array(
        'Index',
        'TopByCountry',
        'Artist',
    ),
    'appTitle' => 'last.fm Client',
    'defaultMeta' => 'last.fm client written by Kieran Wilson',
    'defaultLayout' => 'Default',
    'topnav' => array(
        array(
            'title' => 'Home',
            'href' => constant('publicBaseURL'),
            'content' => 'Home'
        ),
        array(
            'title' => 'About',
            'href' => constant('publicBaseURL')."/about",
            'content' => 'About'
        ),
        array(
            'title' => 'Vist my site',
            'href' => "http://kieranwilson.me",
            'content' => 'More of me'
        )

    )
);
