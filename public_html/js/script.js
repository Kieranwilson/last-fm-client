document.querySelector("#country-form").addEventListener("submit", function(e){
    e.preventDefault();
    countrySearch = this.elements["country"].value;
    if (validateCountry(countrySearch)) {
        encodeUrl = encodeURIComponent(countrySearch);
        location = this.action + '/' + encodeUrl;
    } else
        alert("Countries only accept letters and spaces.")
});

function validateCountry(country) {
    var RegExpression = /^[a-zA-Z\s]*$/;
    if (RegExpression.test(country))
        return true
    else
        return false
}